/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tecleraceweb;

import javax.swing.JOptionPane;

/**
 *
 * @author Jonas
 */
public class Jogo extends javax.swing.JFrame {

    //variáveis globais
    //public int qtdeJogadores;   
    //public String[] tecla;
    //public String[] posicao;
    //public String[] nomes;
    public Jogador[] jogador= new Jogador[8];
    public Servidor servidor;

    //public int[] pontuacao = new int[8];
    
    /**
     * Creates new form Jogo
     */
    public Jogo(Servidor servidor, Jogador[] jogador) {
       
        //gerando variaveis globais
        this.jogador = jogador;
        this.servidor = servidor;
        

        
        validate();
        initComponents();
        
        //limitando barras de progresso
        jProgressBar1.setMaximum(servidor.getNumCliques());
        jProgressBar2.setMaximum(servidor.getNumCliques());
        jProgressBar3.setMaximum(servidor.getNumCliques());
        jProgressBar4.setMaximum(servidor.getNumCliques());
        jProgressBar5.setMaximum(servidor.getNumCliques());
        jProgressBar6.setMaximum(servidor.getNumCliques());
        jProgressBar7.setMaximum(servidor.getNumCliques());
        jProgressBar8.setMaximum(servidor.getNumCliques());
        
        //colocando nomes
        //inserindo nomes + tecla
        try{
            lblJogador1.setText(jogador[0].getNome() + " - Tecla " + jogador[0].getTecla());
            lblJogador2.setText(jogador[1].getNome() + " - Tecla " + jogador[1].getTecla());
            lblJogador3.setText(jogador[2].getNome() + " - Tecla " + jogador[2].getTecla());
            lblJogador4.setText(jogador[3].getNome() + " - Tecla " + jogador[3].getTecla());
        
            lblJogador5.setText(jogador[4].getNome() + " - Tecla " + jogador[4].getTecla());
            lblJogador6.setText(jogador[5].getNome() + " - Tecla " + jogador[5].getTecla());
            lblJogador7.setText(jogador[6].getNome() + " - Tecla " + jogador[6].getTecla());
            lblJogador8.setText(jogador[7].getNome() + " - Tecla " + jogador[7].getTecla());
        }
        catch (Exception e) {}
        
        
        switch (servidor.getQtdeJogadores()) {
            case 2: 
                //desativando progressbars
                jProgressBar3.setVisible(false);
                jProgressBar4.setVisible(false);
                jProgressBar5.setVisible(false);
                jProgressBar6.setVisible(false);
                jProgressBar7.setVisible(false);
                jProgressBar8.setVisible(false);
                lblJogador3.setVisible(false);
                lblJogador4.setVisible(false);
                lblJogador5.setVisible(false);
                lblJogador6.setVisible(false);
                lblJogador7.setVisible(false);
                lblJogador8.setVisible(false);
                lblP3.setVisible(false);
                lblP4.setVisible(false);
                lblP5.setVisible(false);
                lblP6.setVisible(false);
                lblP7.setVisible(false);
                lblP8.setVisible(false);
                lblPontuacao3.setVisible(false);
                lblPontuacao4.setVisible(false);
                lblPontuacao5.setVisible(false);
                lblPontuacao6.setVisible(false);
                lblPontuacao7.setVisible(false);
                lblPontuacao8.setVisible(false);
                break;
                
            case 3:
                //desativando progressbars
                jProgressBar4.setVisible(false);
                jProgressBar5.setVisible(false);
                jProgressBar6.setVisible(false);
                jProgressBar7.setVisible(false);
                jProgressBar8.setVisible(false);
                lblJogador4.setVisible(false);
                lblJogador5.setVisible(false);
                lblJogador6.setVisible(false);
                lblJogador7.setVisible(false);
                lblJogador8.setVisible(false);
                lblP4.setVisible(false);
                lblP5.setVisible(false);
                lblP6.setVisible(false);
                lblP7.setVisible(false);
                lblP8.setVisible(false);
                lblPontuacao4.setVisible(false);
                lblPontuacao5.setVisible(false);
                lblPontuacao6.setVisible(false);
                lblPontuacao7.setVisible(false);
                lblPontuacao8.setVisible(false);
                
                break;
                
            case 4:
                //desativando progressbars
                jProgressBar5.setVisible(false);
                jProgressBar6.setVisible(false);
                jProgressBar7.setVisible(false);
                jProgressBar8.setVisible(false);
                lblJogador5.setVisible(false);
                lblJogador6.setVisible(false);
                lblJogador7.setVisible(false);
                lblJogador8.setVisible(false);
                lblP5.setVisible(false);
                lblP6.setVisible(false);
                lblP7.setVisible(false);
                lblP8.setVisible(false);
                lblPontuacao5.setVisible(false);
                lblPontuacao6.setVisible(false);
                lblPontuacao7.setVisible(false);
                lblPontuacao8.setVisible(false);
                
                break;
                
            case 5:
                //desativando progressbars
                jProgressBar6.setVisible(false);
                jProgressBar7.setVisible(false);
                jProgressBar8.setVisible(false);
                lblJogador6.setVisible(false);
                lblJogador7.setVisible(false);
                lblJogador8.setVisible(false);
                lblP6.setVisible(false);
                lblP7.setVisible(false);
                lblP8.setVisible(false);
                lblPontuacao6.setVisible(false);
                lblPontuacao7.setVisible(false);
                lblPontuacao8.setVisible(false);
                
                break;
                
            case 6:
                //desativando progressbars
                jProgressBar7.setVisible(false);
                jProgressBar8.setVisible(false);
                lblJogador7.setVisible(false);
                lblJogador8.setVisible(false);
                lblP7.setVisible(false);
                lblP8.setVisible(false);
                lblPontuacao7.setVisible(false);
                lblPontuacao8.setVisible(false);
                
                break;
                
            case 7:
                //desativando progressbars
                jProgressBar8.setVisible(false);
                lblJogador8.setVisible(false);
                lblP8.setVisible(false);
                lblPontuacao8.setVisible(false);
                
                break;
                
            case 8:
                break;  
                
        }
    }
    
    
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        lblJogador1 = new javax.swing.JLabel();
        jProgressBar1 = new javax.swing.JProgressBar();
        lblP1 = new javax.swing.JLabel();
        lblJogador2 = new javax.swing.JLabel();
        lblP2 = new javax.swing.JLabel();
        jProgressBar2 = new javax.swing.JProgressBar();
        lblP3 = new javax.swing.JLabel();
        lblJogador3 = new javax.swing.JLabel();
        jProgressBar3 = new javax.swing.JProgressBar();
        jLabel8 = new javax.swing.JLabel();
        lblJogador4 = new javax.swing.JLabel();
        jProgressBar4 = new javax.swing.JProgressBar();
        lblP4 = new javax.swing.JLabel();
        lblJogador5 = new javax.swing.JLabel();
        jProgressBar5 = new javax.swing.JProgressBar();
        lblP5 = new javax.swing.JLabel();
        lblJogador6 = new javax.swing.JLabel();
        jProgressBar6 = new javax.swing.JProgressBar();
        lblP6 = new javax.swing.JLabel();
        lblJogador7 = new javax.swing.JLabel();
        jProgressBar7 = new javax.swing.JProgressBar();
        lblP7 = new javax.swing.JLabel();
        lblJogador8 = new javax.swing.JLabel();
        jProgressBar8 = new javax.swing.JProgressBar();
        lblP8 = new javax.swing.JLabel();
        btnSair = new javax.swing.JButton();
        lblPontuacao1 = new javax.swing.JLabel();
        lblPontuacao2 = new javax.swing.JLabel();
        lblPontuacao3 = new javax.swing.JLabel();
        lblPontuacao4 = new javax.swing.JLabel();
        lblPontuacao5 = new javax.swing.JLabel();
        lblPontuacao6 = new javax.swing.JLabel();
        lblPontuacao7 = new javax.swing.JLabel();
        lblPontuacao8 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Jogo");
        setFocusTraversalPolicyProvider(true);
        addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                formKeyReleased(evt);
            }
        });

        jLabel1.setToolTipText("");

        lblJogador1.setText("Jogador 1 - Tecla X:");

        lblP1.setText("Pontuação:");

        lblJogador2.setText("Jogador 2 - Tecla X:");

        lblP2.setText("Pontuação:");

        lblP3.setText("Pontuação:");

        lblJogador3.setText("Jogador 3 - Tecla X:");

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel8.setText("TecleRace");

        lblJogador4.setText("Jogador 4 - Tecla X:");

        lblP4.setText("Pontuação:");

        lblJogador5.setText("Jogador 5 - Tecla X:");

        lblP5.setText("Pontuação:");

        lblJogador6.setText("Jogador 6 - Tecla X:");

        lblP6.setText("Pontuação:");

        lblJogador7.setText("Jogador 7 - Tecla X:");

        lblP7.setText("Pontuação:");

        lblJogador8.setText("Jogador 8 - Tecla X:");

        lblP8.setText("Pontuação:");

        btnSair.setText("Sair");
        btnSair.setToolTipText("");
        btnSair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSairActionPerformed(evt);
            }
        });
        btnSair.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                btnSairKeyReleased(evt);
            }
        });

        lblPontuacao1.setForeground(new java.awt.Color(51, 51, 255));
        lblPontuacao1.setText("0");

        lblPontuacao2.setForeground(new java.awt.Color(51, 51, 255));
        lblPontuacao2.setText("0");

        lblPontuacao3.setForeground(new java.awt.Color(51, 51, 255));
        lblPontuacao3.setText("0");

        lblPontuacao4.setForeground(new java.awt.Color(51, 51, 255));
        lblPontuacao4.setText("0");

        lblPontuacao5.setForeground(new java.awt.Color(51, 51, 255));
        lblPontuacao5.setText("0");

        lblPontuacao6.setForeground(new java.awt.Color(51, 51, 255));
        lblPontuacao6.setText("0");

        lblPontuacao7.setForeground(new java.awt.Color(51, 51, 255));
        lblPontuacao7.setText("0");

        lblPontuacao8.setForeground(new java.awt.Color(51, 51, 255));
        lblPontuacao8.setText("0");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(182, 182, 182)
                .addComponent(jLabel1)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jProgressBar8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jProgressBar7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jProgressBar6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jProgressBar5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jProgressBar4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jProgressBar3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jProgressBar2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jProgressBar1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblJogador2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(lblJogador6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(lblJogador4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(lblJogador5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(lblJogador7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(lblJogador8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(lblJogador3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(lblJogador1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(lblP2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addGap(18, 18, 18)
                                                .addComponent(lblPontuacao2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(lblP6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addGap(18, 18, 18)
                                                .addComponent(lblPontuacao6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(lblP4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addGap(18, 18, 18)
                                                .addComponent(lblPontuacao4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(lblP5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addGap(18, 18, 18)
                                                .addComponent(lblPontuacao5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(lblP7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addGap(18, 18, 18)
                                                .addComponent(lblPontuacao7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(lblP8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addGap(18, 18, 18)
                                                .addComponent(lblPontuacao8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(lblP3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addGap(18, 18, 18)
                                                .addComponent(lblPontuacao3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(lblP1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addGap(18, 18, 18)
                                                .addComponent(lblPontuacao1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                                        .addGap(17, 17, 17)))
                                .addGap(633, 633, 633))))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(btnSair))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(315, 315, 315)
                                .addComponent(jLabel8)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(9, 9, 9)
                .addComponent(jLabel8)
                .addGap(18, 18, 18)
                .addComponent(lblJogador1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jProgressBar1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblP1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblPontuacao1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addComponent(lblJogador2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jProgressBar2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblP2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblPontuacao2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addComponent(lblJogador3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jProgressBar3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblP3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblPontuacao3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addComponent(lblJogador4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jProgressBar4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblP4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblPontuacao4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addComponent(lblJogador5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jProgressBar5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblP5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblPontuacao5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addComponent(lblJogador6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jProgressBar6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblP6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblPontuacao6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addComponent(lblJogador7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jProgressBar7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblP7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblPontuacao7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addComponent(lblJogador8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jProgressBar8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblP8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblPontuacao8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnSair, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyReleased
        // TODO add your handling code here:
        teclapressionada(evt);
        
    }//GEN-LAST:event_formKeyReleased

    private void btnSairKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnSairKeyReleased
            // TODO add your handling code here:
        teclapressionada(evt);
    }//GEN-LAST:event_btnSairKeyReleased

    private void btnSairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSairActionPerformed
        // TODO add your handling code here:
        this.setVisible(false);
    }//GEN-LAST:event_btnSairActionPerformed

    private void teclapressionada(java.awt.event.KeyEvent evt){
        Integer qtdeJogadores = servidor.getQtdeJogadores();
        
        
        if ( (qtdeJogadores >= 1) ) {
            //se a tecla for do jogador 0
            if (jogador[0].getTecla().equals(evt.getKeyChar() + "")) {
                //se ele alcancou o objetivo
                if (jogador[0].Teclou(servidor.getNumCliques())) {
                    JOptionPane.showMessageDialog(rootPane, "Jogador " + jogador[0].getNome() + " Venceu!");
                }
                jProgressBar1.setValue(jogador[0].getCliques());
                lblPontuacao1.setText(Integer.toString(jogador[0].getCliques()));
            }
        }
        
        if ( (qtdeJogadores >= 2) ) {
            if (jogador[1].getTecla().equals(evt.getKeyChar() + "")) {
                //se ele alcancou o objetivo
                if (jogador[1].Teclou(servidor.getNumCliques())) {
                    JOptionPane.showMessageDialog(rootPane, "Jogador " + jogador[1].getNome() + " Venceu!");
                }
                jProgressBar2.setValue(jogador[1].getCliques());
                lblPontuacao2.setText(Integer.toString(jogador[1].getCliques()));
            }
        }
        
        if ( (qtdeJogadores >= 3) ) {
            if (jogador[2].getTecla().equals (evt.getKeyChar() + "")) {
                //se ele alcancou o objetivo
                if (jogador[2].Teclou(servidor.getNumCliques())) {
                    JOptionPane.showMessageDialog(rootPane, "Jogador " + jogador[2].getNome() + " Venceu!");
                }
                jProgressBar3.setValue(jogador[2].getCliques());
                lblPontuacao3.setText(Integer.toString(jogador[2].getCliques()));     
            }
        }
        
        if ( (qtdeJogadores >= 4) ) {
            if (jogador[3].getTecla().equals (evt.getKeyChar() + "")) {
                //se ele alcancou o objetivo
                if (jogador[3].Teclou(servidor.getNumCliques())) {
                    JOptionPane.showMessageDialog(rootPane, "Jogador " + jogador[3].getNome() + " Venceu!");
                }
                jProgressBar4.setValue(jogador[3].getCliques());
                lblPontuacao4.setText(Integer.toString(jogador[3].getCliques()));     
            }
        }
                
        if ( (qtdeJogadores >= 5) ) {
            if (jogador[4].getTecla().equals (evt.getKeyChar() + "")) {
                //se ele alcancou o objetivo
                if (jogador[4].Teclou(servidor.getNumCliques())) {
                    JOptionPane.showMessageDialog(rootPane, "Jogador " + jogador[4].getNome() + " Venceu!");
                }
                jProgressBar5.setValue(jogador[4].getCliques());
                lblPontuacao5.setText(Integer.toString(jogador[4].getCliques()));    
            }
        }
        
        if ( (qtdeJogadores >= 6) ) {
            if (jogador[5].getTecla().equals (evt.getKeyChar() + "")) {
                //se ele alcancou o objetivo
                if (jogador[5].Teclou(servidor.getNumCliques())) {
                    JOptionPane.showMessageDialog(rootPane, "Jogador " + jogador[5].getNome() + " Venceu!");
                }
                jProgressBar6.setValue(jogador[5].getCliques());
                lblPontuacao6.setText(Integer.toString(jogador[5].getCliques()));     
            }
        }
        
        if ( (qtdeJogadores >= 7) ) {
            if (jogador[6].getTecla().equals (evt.getKeyChar() + "")) {
                //se ele alcancou o objetivo
                if (jogador[6].Teclou(servidor.getNumCliques())) {
                    JOptionPane.showMessageDialog(rootPane, "Jogador " + jogador[6].getNome() + " Venceu!");
                }
                jProgressBar7.setValue(jogador[6].getCliques());
                lblPontuacao7.setText(Integer.toString(jogador[6].getCliques()));     
            }
        }
        
        if ( (qtdeJogadores >= 8) ) {
            if (jogador[7].getTecla().equals (evt.getKeyChar() + "")) {
                //se ele alcancou o objetivo
                if (jogador[7].Teclou(servidor.getNumCliques())) {
                    JOptionPane.showMessageDialog(rootPane, "Jogador " + jogador[7].getNome() + " Venceu!");
                }
                jProgressBar8.setValue(jogador[7].getCliques());
                lblPontuacao8.setText(Integer.toString(jogador[7].getCliques()));     
            }        
        }
        
                    
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /*
         * Set the Nimbus look and feel
         */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /*
         * If Nimbus (introduced in Java SE 6) is not available, stay with the
         * default look and feel. For details see
         * http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Jogo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Jogo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Jogo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Jogo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /*
         * Create and display the form
         */
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
               // new Jogo().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnSair;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JProgressBar jProgressBar1;
    private javax.swing.JProgressBar jProgressBar2;
    private javax.swing.JProgressBar jProgressBar3;
    private javax.swing.JProgressBar jProgressBar4;
    private javax.swing.JProgressBar jProgressBar5;
    private javax.swing.JProgressBar jProgressBar6;
    private javax.swing.JProgressBar jProgressBar7;
    private javax.swing.JProgressBar jProgressBar8;
    private javax.swing.JLabel lblJogador1;
    private javax.swing.JLabel lblJogador2;
    private javax.swing.JLabel lblJogador3;
    private javax.swing.JLabel lblJogador4;
    private javax.swing.JLabel lblJogador5;
    private javax.swing.JLabel lblJogador6;
    private javax.swing.JLabel lblJogador7;
    private javax.swing.JLabel lblJogador8;
    private javax.swing.JLabel lblP1;
    private javax.swing.JLabel lblP2;
    private javax.swing.JLabel lblP3;
    private javax.swing.JLabel lblP4;
    private javax.swing.JLabel lblP5;
    private javax.swing.JLabel lblP6;
    private javax.swing.JLabel lblP7;
    private javax.swing.JLabel lblP8;
    private javax.swing.JLabel lblPontuacao1;
    private javax.swing.JLabel lblPontuacao2;
    private javax.swing.JLabel lblPontuacao3;
    private javax.swing.JLabel lblPontuacao4;
    private javax.swing.JLabel lblPontuacao5;
    private javax.swing.JLabel lblPontuacao6;
    private javax.swing.JLabel lblPontuacao7;
    private javax.swing.JLabel lblPontuacao8;
    // End of variables declaration//GEN-END:variables

   
} 


