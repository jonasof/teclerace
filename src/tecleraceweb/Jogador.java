/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tecleraceweb;

/**
 *
 * @author Jonas
 */
public class Jogador {
    private String nome = "";
    private String tecla = "";
    private Integer cliques = 0;
    private Double velocidade = 0.0;
    
     /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the tecla
     */
    public String getTecla() {
        return tecla;
    }

    /**
     * @param tecla the tecla to set
     */
    public void setTecla(String tecla) {
        this.tecla = tecla;
    }

    /**
     * @return the cliques
     */
    public Integer getCliques() {
        return cliques;
    }

    /**
     * @param cliques the cliques to set
     */
    public void setCliques(Integer cliques) {
        this.cliques = cliques;
    }

    /**
     * @return the velocidade
     */
    public Double getVelocidade() {
        return velocidade;
    }

    /**
     * @param velocidade the velocidade to set
     */
    public void setVelocidade(Double velocidade) {
        this.velocidade = velocidade;
    }
    
    
    
    public Jogador(String nome, String tecla) {
        
        setNome(nome);
        setTecla(tecla);
        
    }
    
    public boolean Teclou(Integer totalTeclas) {
        setCliques(getCliques() + 1);
        if (totalTeclas == getCliques()) {return true;}
        else {return false;}
    }
    
}
