/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tecleraceweb;

/**
 *
 * @author Jonas
 */
public class Servidor {
    private Integer NumCliques;
    private Integer QtdeJogadores;
    private Integer porta = 27020;

    /**
     * @return the NumCliques
     */
    public Integer getNumCliques() {
        return NumCliques;
    }

    /**
     * @param NumCliques the NumCliques to set
     */
    public void setNumCliques(Integer NumCliques) {
        this.NumCliques = NumCliques;
    }

    /**
     * @return the QtdeJogadores
     */
    public Integer getQtdeJogadores() {
        return QtdeJogadores;
    }

    /**
     * @param QtdeJogadores the QtdeJogadores to set
     */
    public void setQtdeJogadores(Integer QtdeJogadores) {
        this.QtdeJogadores = QtdeJogadores;
    }

    /**
     * @return the porta
     */
    public Integer getPorta() {
        return porta;
    }

    /**
     * @param porta the porta to set
     */
    public void setPorta(Integer porta) {
        this.porta = porta;
    }
    
    public Servidor(Integer NumCliques, Integer QtdeJogadores) {
        setNumCliques(NumCliques);
        setQtdeJogadores(QtdeJogadores);
    }
    
    
}
