/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tecleraceweb;

import java.io.*;
import java.net.*;
import javax.swing.JOptionPane;

/**
 *
 * @author Jonas
 */
public class ServidorOnline extends Servidor{
    
    private Jogador[] jogadores = new Jogador[8];
    public ServerSocket socket;
    private Thread conexao;


    /**
     * @return the jogadores
     */
    public Jogador[] getJogadores() {
        return jogadores;
    }

    /**
     * @param jogadores the jogadores to set
     */
    public void setJogadores(Jogador[] jogadores) {
        this.jogadores = jogadores;
    }
    
    public Jogador getJogadorEspecifico(Integer CodJogador) {
        if (CodJogador <= 8) {
            return this.jogadores[CodJogador];
        }
        else{
            Jogador vazio = new Jogador("null", "null");
            return vazio;
        }
    }
    
    public void setJogadorEspecifico(Integer CodJogador, Jogador jogador) {
        jogadores[CodJogador] = jogador;
    }
    
    
    public ServidorOnline(Integer NumCliques, Integer QtdeJogadores, Integer porta){
        super(NumCliques, QtdeJogadores);
        
        //criando servidor
        try{
            socket = new ServerSocket(4000);
            
            conexao = new Thread();
            
            }

        catch(Exception IOException) {
            JOptionPane.showMessageDialog(null, "Não foi possível criar o servidor. Erro:" + IOException.toString());
        }
    }
  

    private void thconexao(){
        
        try{
            Socket sock = socket.accept();
				
            PrintWriter writer = new PrintWriter(sock.getOutputStream());
            String advice = "Você se conectou com sucesso ao meu servidor";
				
            writer.println(advice);
            writer.close();
            System.out.println(advice);
        }
        
        catch(Exception IOException) {
            JOptionPane.showMessageDialog(null, "Não foi possível criar o servidor. Erro:" + IOException.toString());
        }
    
    }
    
    
    
}
